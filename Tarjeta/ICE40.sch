EESchema Schematic File Version 4
LIBS:power
LIBS:74xx
LIBS:Tarjeta-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date "2018-08-31"
Rev ""
Comp "Juli Acero "
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ICE40HX1K-TQ144:ICE40HX1K-TQ144 U?
U 1 1 5B8C7D6A
P 5350 3600
F 0 "U?" H 5450 6085 50  0000 C CNN
F 1 "ICE40HX1K-TQ144" H 5450 5994 50  0000 C CNN
F 2 "QFP50P2200X2200X160-144N" H 5350 3600 50  0001 L BNN
F 3 "Lattice Semiconductor" H 5350 3600 50  0001 L BNN
F 4 "TQFP-144 Lattice Semiconductor" H 5350 3600 50  0001 L BNN "Field4"
F 5 "" H 5350 3600 50  0001 L BNN "Field5"
F 6 "" H 5350 3600 50  0001 L BNN "Field6"
F 7 "HX Series 1280 LUTs 95 I/O 64 kBit RAM 1.2 V Surface Mount FPGA - TQFP-144" H 5350 3600 50  0001 L BNN "Field7"
F 8 "ICE40HX1K-TQ144" H 5350 3600 50  0001 L BNN "Field8"
	1    5350 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C CLF
U 1 1 5B8C8464
P 6650 800
F 0 "CLF" V 6398 800 50  0000 C CNN
F 1 "10uF" V 6489 800 50  0000 C CNN
F 2 "" H 6688 650 50  0001 C CNN
F 3 "~" H 6650 800 50  0001 C CNN
	1    6650 800 
	0    1    1    0   
$EndComp
$Comp
L Device:C CHF
U 1 1 5B8C84DD
P 6650 1200
F 0 "CHF" V 6398 1200 50  0000 C CNN
F 1 "100nF" V 6489 1200 50  0000 C CNN
F 2 "" H 6688 1050 50  0001 C CNN
F 3 "~" H 6650 1200 50  0001 C CNN
	1    6650 1200
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5B8C85A2
P 5500 800
F 0 "R?" V 5293 800 50  0000 C CNN
F 1 "100" V 5384 800 50  0000 C CNN
F 2 "" V 5430 800 50  0001 C CNN
F 3 "~" H 5500 800 50  0001 C CNN
	1    5500 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 1500 5750 800 
Wire Wire Line
	5750 800  5650 800 
Wire Wire Line
	6500 800  6150 800 
Connection ~ 5750 800 
Wire Wire Line
	6500 1200 6150 1200
Wire Wire Line
	6150 1200 6150 800 
Connection ~ 6150 800 
Wire Wire Line
	6150 800  5750 800 
Wire Wire Line
	6800 800  6900 800 
Wire Wire Line
	7250 800  7250 1500
Wire Wire Line
	7250 1500 7150 1500
Wire Wire Line
	6800 1200 6900 1200
Wire Wire Line
	6900 1200 6900 800 
Connection ~ 6900 800 
Wire Wire Line
	6900 800  7250 800 
Wire Wire Line
	5350 800  5250 800 
NoConn ~ 5750 1700
Text GLabel 5200 3000 2    50   Input ~ 0
VCCIO_X
Text GLabel 5000 4700 2    50   Input ~ 0
VCCIO_X
Text GLabel 7900 4700 2    50   Input ~ 0
VCCIO_X
Text GLabel 7750 2750 2    50   Input ~ 0
VCCIO_X
Wire Wire Line
	7350 2750 7350 3000
Connection ~ 7350 3000
Wire Wire Line
	7350 3000 7350 3100
Wire Wire Line
	4950 3000 5050 3000
Wire Wire Line
	4950 3100 5050 3100
Wire Wire Line
	5050 3100 5050 3000
Connection ~ 5050 3000
Wire Wire Line
	5050 3000 5200 3000
Wire Wire Line
	5000 4700 4850 4700
Wire Wire Line
	4750 4800 4850 4800
Wire Wire Line
	4850 4800 4850 4700
Connection ~ 4850 4700
Wire Wire Line
	4850 4700 4750 4700
Wire Wire Line
	7750 4700 7800 4700
Wire Wire Line
	7750 4800 7800 4800
Wire Wire Line
	7800 4800 7800 4700
Connection ~ 7800 4700
Wire Wire Line
	7800 4700 7900 4700
Wire Wire Line
	5750 1800 5550 1800
Text GLabel 5050 950  0    50   Input ~ 0
VCC_FPGA
Wire Wire Line
	5050 950  5250 950 
Wire Wire Line
	5250 950  5250 800 
Text GLabel 5500 2100 0    50   Input ~ 0
VCC_FPGA
Wire Wire Line
	5750 2000 5750 2100
Connection ~ 5750 2100
Wire Wire Line
	5750 2100 5750 2200
Connection ~ 5750 2200
Wire Wire Line
	5750 2200 5750 2300
Wire Wire Line
	5750 2100 5500 2100
Wire Wire Line
	7150 1700 7150 1800
Connection ~ 7150 1800
Wire Wire Line
	7150 1800 7150 1900
Connection ~ 7150 1900
Wire Wire Line
	7150 1900 7150 2000
Connection ~ 7150 2000
Wire Wire Line
	7150 2000 7150 2100
Connection ~ 7150 2100
Wire Wire Line
	7150 2100 7150 2200
Connection ~ 7150 2200
Wire Wire Line
	7150 2200 7150 2300
Connection ~ 7150 2300
Wire Wire Line
	7150 2300 7150 2400
Connection ~ 7150 2400
Wire Wire Line
	7150 2400 7150 2500
$Comp
L power:GND #PWR?
U 1 1 5B8CBF42
P 7350 2100
F 0 "#PWR?" H 7350 1850 50  0001 C CNN
F 1 "GND" H 7355 1927 50  0000 C CNN
F 2 "" H 7350 2100 50  0001 C CNN
F 3 "" H 7350 2100 50  0001 C CNN
	1    7350 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2100 7350 2000
Wire Wire Line
	7350 2000 7150 2000
Text HLabel 3150 2200 0    50   Input ~ 0
ICE_SPI_SCK
Text HLabel 3150 2300 0    50   Input ~ 0
ICE_SPI_MOSI
Text HLabel 3150 2400 0    50   Input ~ 0
ICE_SPI_MOSI
Text HLabel 3150 2500 0    50   Input ~ 0
ICE_SPI_SS
Text GLabel 5750 4900 0    50   Input ~ 0
IOL_1A
Text GLabel 5750 5000 0    50   Input ~ 0
IOL_1B
Text GLabel 5750 5100 0    50   Input ~ 0
IOL_2A
Text GLabel 5750 5200 0    50   Input ~ 0
IOL_2B
Text GLabel 5750 5300 0    50   Input ~ 0
IOL_3A
Text GLabel 5750 5400 0    50   Input ~ 0
IOL_3B
Text GLabel 5750 5500 0    50   Input ~ 0
IOL_4A
Text GLabel 5750 5600 0    50   Input ~ 0
IOL_4B
Text GLabel 5750 5700 0    50   Input ~ 0
IOL_5A
Text GLabel 5750 5800 0    50   Input ~ 0
IOL_5B
Text GLabel 5750 5900 0    50   Input ~ 0
IOL_6A
Text GLabel 5750 6000 0    50   Input ~ 0
IOL_7B
Text GLabel 7750 5000 2    50   Input ~ 0
IOL_8A
Text GLabel 7750 5100 2    50   Input ~ 0
IOL_8B
Text GLabel 7750 5200 2    50   Input ~ 0
IOL_9A
Text GLabel 7750 5300 2    50   Input ~ 0
IOL_9B
Text GLabel 7750 5400 2    50   Input ~ 0
IOL_10A
Text GLabel 7750 5500 2    50   Input ~ 0
IOL_10B
Text GLabel 7750 5600 2    50   Input ~ 0
IOL_11A
Text GLabel 7750 5700 2    50   Input ~ 0
IOL_11B
Text GLabel 7750 5800 2    50   Input ~ 0
IOL_12A
Text GLabel 7750 5900 2    50   Input ~ 0
IOL_12B
Text GLabel 8950 4750 0    50   Input ~ 0
IOL_1A
Text GLabel 8950 4850 0    50   Input ~ 0
IOL_1B
Text GLabel 8950 4950 0    50   Input ~ 0
IOL_2A
Text GLabel 8950 5050 0    50   Input ~ 0
IOL_2B
Text GLabel 8950 5150 0    50   Input ~ 0
IOL_3A
Text GLabel 8950 5250 0    50   Input ~ 0
IOL_3B
Text GLabel 8950 5350 0    50   Input ~ 0
IOL_4A
Text GLabel 8950 5450 0    50   Input ~ 0
IOL_4B
Text GLabel 8950 5550 0    50   Input ~ 0
IOL_5A
Text GLabel 8950 5750 0    50   Input ~ 0
IOL_6A
Text GLabel 9450 5750 2    50   Input ~ 0
IOL_7B
Text GLabel 9450 5650 2    50   Input ~ 0
IOL_8A
Text GLabel 9450 5550 2    50   Input ~ 0
IOL_8B
Text GLabel 9450 5450 2    50   Input ~ 0
IOL_9A
Text GLabel 9450 5350 2    50   Input ~ 0
IOL_9B
Text GLabel 9450 5250 2    50   Input ~ 0
IOL_10A
Text GLabel 9450 5150 2    50   Input ~ 0
IOL_10B
Text GLabel 9450 5050 2    50   Input ~ 0
IOL_11A
Text GLabel 9450 4950 2    50   Input ~ 0
IOL_11B
Text GLabel 8950 5650 0    50   Input ~ 0
IOL_5B
Text GLabel 9450 4850 2    50   Input ~ 0
IOL_12A
Text GLabel 9450 4750 2    50   Input ~ 0
IOL_12B
$Comp
L power:GND #PWR?
U 1 1 5B9A192D
P 10000 4700
F 0 "#PWR?" H 10000 4450 50  0001 C CNN
F 1 "GND" H 10005 4527 50  0000 C CNN
F 2 "" H 10000 4700 50  0001 C CNN
F 3 "" H 10000 4700 50  0001 C CNN
	1    10000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4700 10000 4650
$Comp
L Connector_Generic:Conn_02x13_Counter_Clockwise J?
U 1 1 5B9A2E62
P 9150 5150
F 0 "J?" H 9200 5967 50  0000 C CNN
F 1 "Conn_02x13_Counter_Clockwise" H 9200 5876 50  0000 C CNN
F 2 "" H 9150 5150 50  0001 C CNN
F 3 "~" H 9150 5150 50  0001 C CNN
	1    9150 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4650 9900 4650
Wire Wire Line
	9900 4650 9900 4100
Wire Wire Line
	9900 4100 8500 4100
Wire Wire Line
	8500 4100 8500 4650
Wire Wire Line
	8500 4650 8950 4650
Connection ~ 9900 4650
Wire Wire Line
	9900 4650 10000 4650
Text GLabel 5750 3700 0    50   Input ~ 0
IOB_24
Text GLabel 5750 3800 0    50   Input ~ 0
IOB_25
Text GLabel 5750 3900 0    50   Input ~ 0
IOB_26
Text GLabel 5750 4000 0    50   Input ~ 0
IOB_27
Text GLabel 5750 4100 0    50   Input ~ 0
IOB_28
Text GLabel 5750 4200 0    50   Input ~ 0
IOB_29
Text GLabel 7350 3300 2    50   Input ~ 0
IOB_30
Text GLabel 7350 3400 2    50   Input ~ 0
IOB_31
Text GLabel 7350 3500 2    50   Input ~ 0
IOB_32
Text GLabel 9050 3000 2    50   Input ~ 0
IOB_33
Text GLabel 5750 3300 0    50   Input ~ 0
IOB_34
Text GLabel 7350 3700 2    50   Input ~ 0
IOB_37
Text GLabel 5750 3600 0    50   Input ~ 0
IOB_38
Text GLabel 7350 3800 2    50   Input ~ 0
IOB_39
Text GLabel 7350 3900 2    50   Input ~ 0
IOB_40
Text GLabel 7350 4000 2    50   Input ~ 0
IOB_41
$Comp
L Connector_Generic:Conn_02x10_Counter_Clockwise J?
U 1 1 5B9B4443
P 8750 2600
F 0 "J?" H 8800 3217 50  0000 C CNN
F 1 "Conn_02x10_Counter_Clockwise" H 8800 3126 50  0000 C CNN
F 2 "" H 8750 2600 50  0001 C CNN
F 3 "~" H 8750 2600 50  0001 C CNN
	1    8750 2600
	1    0    0    -1  
$EndComp
Text GLabel 8550 2400 0    50   Input ~ 0
IOB_24
Text GLabel 8550 2500 0    50   Input ~ 0
IOB_25
Text GLabel 8550 2600 0    50   Input ~ 0
IOB_26
Text GLabel 8550 2700 0    50   Input ~ 0
IOB_27
Wire Wire Line
	7750 2750 7350 2750
Text GLabel 8550 2800 0    50   Input ~ 0
IOB_28
Text GLabel 8550 2900 0    50   Input ~ 0
IOB_29
Text GLabel 8550 3000 0    50   Input ~ 0
IOB_30
Text GLabel 8550 3100 0    50   Input ~ 0
IOB_31
Text GLabel 9050 3100 2    50   Input ~ 0
IOB_32
Text GLabel 7350 3600 2    50   Input ~ 0
IOB_33
Text GLabel 9050 2800 2    50   Input ~ 0
IOB_37
Text GLabel 9050 2600 2    50   Input ~ 0
IOB_39
Text GLabel 9050 2500 2    50   Input ~ 0
IOB_40
Text GLabel 9050 2400 2    50   Input ~ 0
IOB_41
Text GLabel 9050 2900 2    50   Input ~ 0
IOB_34
Text GLabel 9050 2700 2    50   Input ~ 0
IOB_38
$Comp
L power:GND #PWR?
U 1 1 5B9B700A
P 9450 2350
F 0 "#PWR?" H 9450 2100 50  0001 C CNN
F 1 "GND" H 9455 2177 50  0000 C CNN
F 2 "" H 9450 2350 50  0001 C CNN
F 3 "" H 9450 2350 50  0001 C CNN
	1    9450 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 2350 9450 2300
Wire Wire Line
	9450 2300 9050 2300
$Comp
L power:GND #PWR?
U 1 1 5B9B7C44
P 8100 2350
F 0 "#PWR?" H 8100 2100 50  0001 C CNN
F 1 "GND" H 8105 2177 50  0000 C CNN
F 2 "" H 8100 2350 50  0001 C CNN
F 3 "" H 8100 2350 50  0001 C CNN
	1    8100 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2350 8100 2300
Wire Wire Line
	8100 2300 8550 2300
Text GLabel 4750 5900 2    50   Input ~ 0
IOR_48
Text GLabel 4750 6100 2    50   Input ~ 0
IOR_49
Text GLabel 3150 5100 0    50   Input ~ 0
IOR_52
Text GLabel 3150 5200 0    50   Input ~ 0
IOR_53
Text GLabel 3150 5300 0    50   Input ~ 0
IOR_54
Text GLabel 4750 5800 2    50   Input ~ 0
IOR_50
Text GLabel 4750 6000 2    50   Input ~ 0
IOR_51
NoConn ~ 3150 4700
Text GLabel 3150 5400 0    50   Input ~ 0
IOR_55
Text GLabel 3150 5500 0    50   Input ~ 0
IOR_56
Text GLabel 3150 5600 0    50   Input ~ 0
IOR_57
Text GLabel 3150 5700 0    50   Input ~ 0
IOR_58
Text GLabel 3150 5800 0    50   Input ~ 0
IOR_59
Text GLabel 3150 5900 0    50   Input ~ 0
IOR_62
Text GLabel 3150 6000 0    50   Input ~ 0
IOR_63
Text GLabel 3150 6100 0    50   Input ~ 0
IOR_64
Text GLabel 4750 5000 2    50   Input ~ 0
IOR_65
Text GLabel 4750 5100 2    50   Input ~ 0
IOR_66
Text GLabel 4750 5200 2    50   Input ~ 0
IOR_67
Text GLabel 4750 5300 2    50   Input ~ 0
IOR_68
Text GLabel 4750 5400 2    50   Input ~ 0
IOR_69
Text GLabel 4750 5500 2    50   Input ~ 0
IOR_70
Text GLabel 4750 5600 2    50   Input ~ 0
IOR_71
Text GLabel 4750 5700 2    50   Input ~ 0
IOR_72
$Comp
L Connector_Generic:Conn_02x13_Counter_Clockwise J?
U 1 1 5B9BB67E
P 1600 4800
F 0 "J?" H 1650 5617 50  0000 C CNN
F 1 "Conn_02x13_Counter_Clockwise" H 1650 5526 50  0000 C CNN
F 2 "" H 1600 4800 50  0001 C CNN
F 3 "~" H 1600 4800 50  0001 C CNN
	1    1600 4800
	1    0    0    -1  
$EndComp
Text GLabel 1400 4300 0    50   Input ~ 0
IOR_48
Text GLabel 1400 4400 0    50   Input ~ 0
IOR_49
Text GLabel 1400 4500 0    50   Input ~ 0
IOR_50
Text GLabel 1400 4600 0    50   Input ~ 0
IOR_51
Text GLabel 1400 4700 0    50   Input ~ 0
IOR_52
Text GLabel 1400 4800 0    50   Input ~ 0
IOR_53
Text GLabel 1400 4900 0    50   Input ~ 0
IOR_54
Text GLabel 1400 5000 0    50   Input ~ 0
IOR_55
Text GLabel 1400 5100 0    50   Input ~ 0
IOR_56
Text GLabel 1400 5200 0    50   Input ~ 0
IOR_57
Text GLabel 1400 5300 0    50   Input ~ 0
IOR_58
Text GLabel 1400 5400 0    50   Input ~ 0
IOR_59
Text GLabel 1900 5400 2    50   Input ~ 0
IOR_62
Text GLabel 1900 5300 2    50   Input ~ 0
IOR_63
Text GLabel 1900 5200 2    50   Input ~ 0
IOR_64
Text GLabel 1900 5100 2    50   Input ~ 0
IOR_65
Text GLabel 1900 5000 2    50   Input ~ 0
IOR_66
Text GLabel 1900 4900 2    50   Input ~ 0
IOR_67
Text GLabel 1900 4800 2    50   Input ~ 0
IOR_68
Text GLabel 1900 4700 2    50   Input ~ 0
IOR_69
Text GLabel 1900 4600 2    50   Input ~ 0
IOR_70
Text GLabel 1900 4500 2    50   Input ~ 0
IOR_71
Text GLabel 1900 4400 2    50   Input ~ 0
IOR_72
$Comp
L power:GND #PWR?
U 1 1 5B9BD2EA
P 2350 4350
F 0 "#PWR?" H 2350 4100 50  0001 C CNN
F 1 "GND" H 2355 4177 50  0000 C CNN
F 2 "" H 2350 4350 50  0001 C CNN
F 3 "" H 2350 4350 50  0001 C CNN
	1    2350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 4350 2350 4300
Wire Wire Line
	2350 4300 1900 4300
Text GLabel 2550 1300 2    50   Input ~ 0
VCCIO_X
$Comp
L power:+3.3V #PWR?
U 1 1 5BA04945
P 2300 1250
F 0 "#PWR?" H 2300 1100 50  0001 C CNN
F 1 "+3.3V" H 2315 1423 50  0000 C CNN
F 2 "" H 2300 1250 50  0001 C CNN
F 3 "" H 2300 1250 50  0001 C CNN
	1    2300 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 1250 2300 1300
Wire Wire Line
	2300 1300 2550 1300
Text GLabel 5550 1800 0    50   Input ~ 0
VPP_2V5
Text GLabel 2050 1650 2    50   Input ~ 0
VCC_FPGA
Text GLabel 9050 2200 2    50   Input ~ 0
VCCIO_X
Text GLabel 8550 2200 0    50   Input ~ 0
VCCIO_X
Text GLabel 8950 4550 0    50   Input ~ 0
VCCIO_X
Text GLabel 9450 4550 2    50   Input ~ 0
VCCIO_X
Text GLabel 1900 4200 2    50   Input ~ 0
VCCIO_X
Text GLabel 1400 4200 0    50   Input ~ 0
VCCIO_X
Text GLabel 3300 1550 0    50   Input ~ 0
VCCIO_X
$Comp
L pspice:DIODE D?
U 1 1 5BA06727
P 3500 1550
F 0 "D?" H 3500 1815 50  0000 C CNN
F 1 "DIODE" H 3500 1724 50  0000 C CNN
F 2 "" H 3500 1550 50  0001 C CNN
F 3 "" H 3500 1550 50  0001 C CNN
	1    3500 1550
	1    0    0    -1  
$EndComp
Text GLabel 3700 1550 2    50   Input ~ 0
VPP_2V5
Text GLabel 4750 2200 2    50   Input ~ 0
VCC_SPI
Wire Wire Line
	4750 1950 4750 2200
$Comp
L power:+3.3V #PWR?
U 1 1 5BA0775A
P 4750 1950
F 0 "#PWR?" H 4750 1800 50  0001 C CNN
F 1 "+3.3V" H 4765 2123 50  0000 C CNN
F 2 "" H 4750 1950 50  0001 C CNN
F 3 "" H 4750 1950 50  0001 C CNN
	1    4750 1950
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:TLV1117-15 U?
U 1 1 5BA082A1
P 1300 1650
F 0 "U?" H 1300 1892 50  0000 C CNN
F 1 "TLV1117-15" H 1300 1801 50  0000 C CNN
F 2 "" H 1300 1650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlv1117.pdf" H 1300 1650 50  0001 C CNN
	1    1300 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+1V2 #PWR?
U 1 1 5BA0836C
P 1800 1500
F 0 "#PWR?" H 1800 1350 50  0001 C CNN
F 1 "+1V2" H 1815 1673 50  0000 C CNN
F 2 "" H 1800 1500 50  0001 C CNN
F 3 "" H 1800 1500 50  0001 C CNN
	1    1800 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1500 1800 1650
Wire Wire Line
	1800 1650 2050 1650
Wire Wire Line
	1600 1650 1700 1650
Connection ~ 1800 1650
$Comp
L power:+3.3V #PWR?
U 1 1 5BA0A44C
P 750 1550
F 0 "#PWR?" H 750 1400 50  0001 C CNN
F 1 "+3.3V" H 765 1723 50  0000 C CNN
F 2 "" H 750 1550 50  0001 C CNN
F 3 "" H 750 1550 50  0001 C CNN
	1    750  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1550 750  1650
Wire Wire Line
	750  1650 850  1650
$Comp
L power:GND #PWR?
U 1 1 5BA0B6FC
P 1300 2100
F 0 "#PWR?" H 1300 1850 50  0001 C CNN
F 1 "GND" H 1305 1927 50  0000 C CNN
F 2 "" H 1300 2100 50  0001 C CNN
F 3 "" H 1300 2100 50  0001 C CNN
	1    1300 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2100 1300 2050
$Comp
L Device:C C?
U 1 1 5BA0CA9E
P 850 1900
F 0 "C?" H 965 1946 50  0000 L CNN
F 1 "1uF" H 965 1855 50  0000 L CNN
F 2 "" H 888 1750 50  0001 C CNN
F 3 "~" H 850 1900 50  0001 C CNN
	1    850  1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5BA0CACE
P 1700 1900
F 0 "C?" H 1815 1946 50  0000 L CNN
F 1 "1uF" H 1815 1855 50  0000 L CNN
F 2 "" H 1738 1750 50  0001 C CNN
F 3 "~" H 1700 1900 50  0001 C CNN
	1    1700 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2050 1300 2050
Connection ~ 1300 2050
Wire Wire Line
	1300 2050 1300 1950
Wire Wire Line
	850  2050 1300 2050
Wire Wire Line
	850  1750 850  1650
Connection ~ 850  1650
Wire Wire Line
	850  1650 1000 1650
Connection ~ 1700 1650
Wire Wire Line
	1700 1650 1800 1650
Wire Wire Line
	1700 1650 1700 1750
Text HLabel 3150 3200 0    50   Input ~ 0
ICE40_NOE
Text HLabel 3150 3300 0    50   Input ~ 0
ICE40_NWE
Text HLabel 3150 3400 0    50   Input ~ 0
ICE40_NE1
Text HLabel 3150 3500 0    50   Input ~ 0
ICE40_NL_NADV
Text GLabel 3150 3600 0    50   Input ~ 0
IOT_77
Text GLabel 3150 3700 0    50   Input ~ 0
IOT_78
Text GLabel 3150 3800 0    50   Input ~ 0
IOT_79
Text GLabel 3150 3900 0    50   Input ~ 0
IOT_80
Text GLabel 3150 4000 0    50   Input ~ 0
IOT_81
Text GLabel 3150 4100 0    50   Input ~ 0
IOT_82
Text GLabel 3150 4200 0    50   Input ~ 0
IOT_83
Text GLabel 4950 3300 2    50   Input ~ 0
IOT_87
Text GLabel 4950 3400 2    50   Input ~ 0
IOT_88
Text GLabel 4950 3500 2    50   Input ~ 0
IOT_89
Text GLabel 4950 3600 2    50   Input ~ 0
IOT_90
Text GLabel 4950 3700 2    50   Input ~ 0
IOT_91
Text GLabel 4950 3800 2    50   Input ~ 0
IOT_92
Text GLabel 4950 3900 2    50   Input ~ 0
IOT_93
Text GLabel 4950 4000 2    50   Input ~ 0
IOT_94
Text GLabel 4950 4100 2    50   Input ~ 0
IOT_95
Text GLabel 4950 4200 2    50   Input ~ 0
IOT_96
$Comp
L Connector_Generic:Conn_02x10_Counter_Clockwise J?
U 1 1 5BA1B800
P 1250 3000
F 0 "J?" H 1300 3617 50  0000 C CNN
F 1 "Conn_02x10_Counter_Clockwise" H 1300 3526 50  0000 C CNN
F 2 "" H 1250 3000 50  0001 C CNN
F 3 "~" H 1250 3000 50  0001 C CNN
	1    1250 3000
	1    0    0    -1  
$EndComp
Text GLabel 1050 2700 0    50   Input ~ 0
IOT_77
Text GLabel 1050 2800 0    50   Input ~ 0
IOT_78
Text GLabel 1050 2900 0    50   Input ~ 0
IOT_79
Text GLabel 1050 3000 0    50   Input ~ 0
IOT_80
Text GLabel 1050 3100 0    50   Input ~ 0
IOT_81
Text GLabel 1050 3200 0    50   Input ~ 0
IOT_82
Text GLabel 1050 3300 0    50   Input ~ 0
IOT_83
Text GLabel 1050 3400 0    50   Input ~ 0
IOT_87
Text GLabel 1050 3500 0    50   Input ~ 0
IOT_88
Text GLabel 1550 3500 2    50   Input ~ 0
IOT_89
Text GLabel 1550 3400 2    50   Input ~ 0
IOT_90
Text GLabel 1550 3300 2    50   Input ~ 0
IOT_91
Text GLabel 1550 3200 2    50   Input ~ 0
IOT_92
Text GLabel 1550 3100 2    50   Input ~ 0
IOT_93
Text GLabel 1550 3000 2    50   Input ~ 0
IOT_94
Text GLabel 1550 2900 2    50   Input ~ 0
IOT_95
Text GLabel 1550 2800 2    50   Input ~ 0
IOT_96
Text GLabel 1050 2600 0    50   Input ~ 0
VCCIO_X
Text GLabel 1550 2600 2    50   Input ~ 0
VCCIO_X
$Comp
L power:GND #PWR?
U 1 1 5BA1BE67
P 2000 2750
F 0 "#PWR?" H 2000 2500 50  0001 C CNN
F 1 "GND" H 2005 2577 50  0000 C CNN
F 2 "" H 2000 2750 50  0001 C CNN
F 3 "" H 2000 2750 50  0001 C CNN
	1    2000 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2750 2000 2700
Wire Wire Line
	2000 2700 1550 2700
$EndSCHEMATC
